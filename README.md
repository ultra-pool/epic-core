Epic Core integration/staging repository
=================================================

Epic (EPIC) is an open-source cryptocurrency project. 

### Coin Specs
|                             |                   |
|-----------------------------|-------------------|
| Max Supply                  |  EPIC  |
| Algorithm                   | Quark             |
| Block Time                  | 60 Seconds        |
| Mature Time                 | 1 hour            |
| Masternode Collateral       | 10,000 EPIC      |


### Reward Distribution

#### Pre-Mine Phase for swap


#### Block reward


### Block Reward Split (Proof-of-Stake Phase)

